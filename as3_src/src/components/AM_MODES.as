package components 
{
    public class AM_MODES
    {
        public static const ADD: String = "add";
        public static const EDIT: String = "edit";
        public static const SUBMIT: String = "submit";
        public static const DELETE: String = "delete";
    }
}
